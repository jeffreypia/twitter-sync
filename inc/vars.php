<?php

$options = get_option('twitter_sync_option');

// Define constants for Twitter app and page info
define( 'OAUTH_ACCESS_TOKEN',			$options['access_token'] );
define( 'OAUTH_ACCESS_TOKEN_SECRET',	$options['access_token_secret'] );
define( 'CONSUMER_KEY',					$options['api_key'] );
define( 'CONSUMER_SECRET',				$options['api_secret'] );
define( 'TWITTER_USERNAME',				$options['twitter_username'] );

?>