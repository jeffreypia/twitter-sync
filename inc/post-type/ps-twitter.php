<?php

add_action( 'init', 'register_cpt_ps_twitter' );

function register_cpt_ps_twitter() {

    $labels = array( 
        'name' => _x( 'Tweets', 'ps_twitter' ),
        'singular_name' => _x( 'Tweet', 'ps_twitter' ),
        'add_new' => _x( 'Add New Tweet', 'ps_twitter' ),
        'all_items' => _x( 'Tweets', 'ps_twitter' ),
        'add_new_item' => _x( 'Add New Tweet', 'ps_twitter' ),
        'edit_item' => _x( 'Edit Tweet', 'ps_twitter' ),
        'new_item' => _x( 'New Tweet', 'ps_twitter' ),
        'view_item' => _x( 'View Tweet', 'ps_twitter' ),
        'search_items' => _x( 'Search Tweets', 'ps_twitter' ),
        'not_found' => _x( 'No Tweets found', 'ps_twitter' ),
        'not_found_in_trash' => _x( 'No Tweets found in Trash', 'ps_twitter' ),
        'parent_item_colon' => _x( 'Parent Tweet:', 'ps_twitter' ),
        'menu_name' => _x( 'Twitter', 'ps_twitter' ),
    );

    $supports = array(
        'title',
        'editor',
        'custom-fields'
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        'supports' => $supports,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'exclude_from_search' => true,
        'menu_position' => 12,
        'menu_icon' => 'dashicons-twitter',
    );

    register_post_type( 'ps_twitter', $args );
}
