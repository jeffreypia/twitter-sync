<?php
class TwitterSyncPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_twitter_sync_page' ) );
        add_action( 'admin_init', array( $this, 'twitter_sync_init' ) );
    }

    /**
     * Add options page
     */
    public function add_twitter_sync_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Post Sync Options: Twitter', 
            'Post Sync Options: Twitter', 
            'manage_options', 
            'twitter-sync-admin', 
            array( $this, 'create_twitter_sync_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_twitter_sync_page()
    {
        // Set class property
        $this->options = get_option( 'twitter_sync_option' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Twitter Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'twitter_sync_options' );   
                do_settings_sections( 'twitter-sync-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function twitter_sync_init()
    {        
        register_setting(
            'twitter_sync_options',
            'twitter_sync_option',
            array( $this, 'sanitize' )
        );

        add_settings_section(
            'twitter_sync_options_section',
            'My Custom Settings',
            array( $this, 'print_section_info' ),
            'twitter-sync-admin'
        );  

        add_settings_field(
            'api_key', 
            'API Key', 
            array( $this, 'api_key_callback' ), 
            'twitter-sync-admin', 
            'twitter_sync_options_section'
        );      

        add_settings_field(
            'api_secret',
            'API Secret', 
            array( $this, 'api_secret_callback' ),
            'twitter-sync-admin',
            'twitter_sync_options_section'
        );      

        add_settings_field(
            'access_token',
            'Access Token', 
            array( $this, 'access_token_callback' ),
            'twitter-sync-admin',
            'twitter_sync_options_section'
        );      

        add_settings_field(
            'access_token_secret',
            'Access Token Secret', 
            array( $this, 'access_token_secret_callback' ),
            'twitter-sync-admin',
            'twitter_sync_options_section'
        );      

        add_settings_field(
            'twitter_username',
            'Twitter Username', 
            array( $this, 'twitter_username_callback' ),
            'twitter-sync-admin',
            'twitter_sync_options_section'
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['api_secret'] ) )
            $new_input['api_secret'] = sanitize_text_field( $input['api_secret'] );

        if( isset( $input['api_key'] ) )
            $new_input['api_key'] = sanitize_text_field( $input['api_key'] );

        if( isset( $input['access_token'] ) )
            $new_input['access_token'] = sanitize_text_field( $input['access_token'] );

        if( isset( $input['access_token_secret'] ) )
            $new_input['access_token_secret'] = sanitize_text_field( $input['access_token_secret'] );

        if( isset( $input['twitter_username'] ) )
            $new_input['twitter_username'] = sanitize_text_field( $input['twitter_username'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your API Keys below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function api_secret_callback()
    {
        printf(
            '<input type="text" id="api_secret" name="twitter_sync_option[api_secret]" value="%s" />',
            isset( $this->options['api_secret'] ) ? esc_attr( $this->options['api_secret']) : ''
        );
    }

    public function api_key_callback()
    {
        printf(
            '<input type="text" id="api_key" name="twitter_sync_option[api_key]" value="%s" />',
            isset( $this->options['api_key'] ) ? esc_attr( $this->options['api_key']) : ''
        );
    }

    public function access_token_callback()
    {
        printf(
            '<input type="text" id="access_token" name="twitter_sync_option[access_token]" value="%s" />',
            isset( $this->options['access_token'] ) ? esc_attr( $this->options['access_token']) : ''
        );
    }

    public function access_token_secret_callback()
    {
        printf(
            '<input type="text" id="access_token_secret" name="twitter_sync_option[access_token_secret]" value="%s" />',
            isset( $this->options['access_token_secret'] ) ? esc_attr( $this->options['access_token_secret']) : ''
        );
    }

    public function twitter_username_callback()
    {
        printf(
            '<input type="text" id="twitter_username" name="twitter_sync_option[twitter_username]" value="%s" />',
            isset( $this->options['twitter_username'] ) ? esc_attr( $this->options['twitter_username']) : ''
        );
    }
}

if( is_admin() )
    $twitter_sync_page = new TwitterSyncPage();

?>