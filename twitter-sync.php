<?php
/**
 * Plugin Name: Twitter Sync
 * Description: Imports tweets into WordPress as custom posts
 * Author: Jeffrey Pia
 * Author URI: http://jeffreypia.com
 * Version: 0.1
 * License: MIT
 */

/* ==========================================================================
	REQUIRES
   ========================================================================== */

// base path
$base = plugin_dir_path( __FILE__ ) . "inc";

require_once("$base/vars.php");

// Register post type
require_once("$base/post-type/ps-twitter.php");

// Create admin page for Twitter settings
require_once("$base/admin.php");

// Reference Twitter PHP API 
// https://github.com/J7mbo/twitter-api-php
require_once("$base/vendor/TwitterAPIExchange.php");

// Create custom interval for cron job
add_filter( 'cron_schedules', 'twitter_sync_add_scheduled_interval' );
function twitter_sync_add_scheduled_interval($schedules) {
	$schedules['twitter_sync_15_mins'] = array(
		'interval' => 15*60,
		'display' => '15 minutes',
	);
	
	return $schedules;
}

// On plugin activation, clear any leftover cron jobs and schedule new cron job
register_activation_hook( __FILE__, 'twitter_sync_plugin_activation' );
function twitter_sync_plugin_activation() {
	wp_clear_scheduled_hook( 'sync_twitter' );
	wp_schedule_event( time(), 'twitter_sync_15_mins', 'sync_twitter' );
}

// On deactivation, clear cron jobs
register_deactivation_hook( __FILE__, 'twitter_sync_plugin_deactivation');
function twitter_sync_plugin_deactivation() {
	wp_clear_scheduled_hook( 'sync_twitter' );
}

// Cron job
// Check DB for most recent post's date
// Query Twitter for all updates after that date
// Add create new custom post for each new update
add_action( 'sync_twitter', 'twitter_sync_start_post_sync' );
function twitter_sync_start_post_sync() {
	global $wpdb;
	
	$twitter = twitter_sync_get_twitter_api();

	// Get ID from most recent posted tweet
	$add_seconds_server = date('Z');
	$lastPostTwitter = $wpdb->get_var("SELECT meta_value FROM $wpdb->posts INNER JOIN $wpdb->postmeta WHERE post_status = 'publish' AND post_type = 'ps_twitter' AND meta_key = 'twitter_sync_id' ORDER BY post_date_gmt DESC LIMIT 1");

	// Query Twitter for tweets after most recent posted tweet
	$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
	$getfield = '?screen_name='.TWITTER_USERNAME;
	$getfield .= $lastPostTwitter ? '&since_id='.$lastPostTwitter : '';
	$requestMethod = 'GET';

	$json = $twitter->setGetfield($getfield)
			->buildOauth($url, $requestMethod)
			->performRequest();
	$response = json_decode($json);

	// foreach through assets and insert to WordPress
	$added_post_ids = array();
	foreach ( $response as $postSocial ) {
		$added_post_ids[] = twitter_sync_add_post( $postSocial );
	}
	return $added_post_ids;
}

function twitter_sync_add_post( $postSocial ) {
	global $wpdb;

	$postSocialID = sanitize_text_field( $postSocial->id_str );

	// Check if Tweet ID exists in WP
	$postID = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON post_id = ID WHERE post_type = 'ps_twitter' AND meta_key = 'twitter_sync_id' AND meta_value = %s LIMIT 1", $postSocialID ) );

	// If not, create new WP post
	if ( ! $postID ) {
		// Twitter API occassionally truncates retweets so, if possible,
		// manually re-construct retweet with the original tweet text
		if(isset($postSocial->retweeted_status)) {
			$cleanMessage = 'RT @' . sanitize_text_field( $postSocial->retweeted_status->user->screen_name ) . ': ' . sanitize_text_field( $postSocial->retweeted_status->text );
		} else {
			$cleanMessage = sanitize_text_field( $postSocial->text );
		}

		$cleanDate = sanitize_text_field( $postSocial->created_at );
		
		$date = new DateTime($cleanDate);
		$utc_date = $date->format('Y-m-d H:i:s');

		$date->setTimeZone(new DateTimeZone('America/New_York'));
		$pub_date = $date->format('Y-m-d H:i:s');

		// If Tweet is empty, do not create new post
		if ( ! $cleanMessage ) {
			return false;
		}

		$newPost = array(
			'post_title'      => $cleanMessage,
			'post_content'    => $cleanMessage,
			'post_status'     => 'publish',
			'ping_status'     => 'closed',
			'comment_status'  => 'closed',
			'post_type'       => 'ps_twitter',
			'post_date'       => $pub_date,
			'post_date_gmt'   => $utc_date,
			'post_author'     => 1
		);

		$postID = wp_insert_post( $newPost, true );

		if ( ! $postID  ) {
			return false;
		}

		update_post_meta( $postID, 'twitter_raw', serialize($postSocial) );
		update_post_meta( $postID, 'twitter_sync_id', $postSocialID );
		update_post_meta( $postID, 'twitter_sync_source', 'Twitter' );
		if(isset($postSocial->retweet_count))				update_post_meta( $postID, 'twitter_sync_retweet_count', 				sanitize_text_field( $postSocial->retweet_count ) );
		if(isset($postSocial->truncated))					update_post_meta( $postID, 'twitter_sync_truncated', 					sanitize_text_field( $postSocial->truncated ) );
		if(isset($postSocial->twitter_source))				update_post_meta( $postID, 'twitter_sync_twitter_source', 				sanitize_text_field( $postSocial->twitter_source ) );
		if(isset($postSocial->in_reply_to_status_id_str))	update_post_meta( $postID, 'twitter_sync_in_reply_to_status_id_str', 	sanitize_text_field( $postSocial->in_reply_to_status_id_str ) );
		if(isset($postSocial->in_reply_to_user_id_str))   	update_post_meta( $postID, 'twitter_sync_in_reply_to_user_id_str', 		sanitize_text_field( $postSocial->in_reply_to_user_id_str ) );
		if(isset($postSocial->in_reply_to_screen_name))   	update_post_meta( $postID, 'twitter_sync_in_reply_to_screen_name', 		sanitize_text_field( $postSocial->in_reply_to_screen_name ) );
		if(isset($postSocial->favorite_count))       		update_post_meta( $postID, 'twitter_sync_favorite_count', 				sanitize_text_field( $postSocial->favorite_count ) );
	}

	return $postID;
}

function twitter_sync_get_twitter_api() {
	static $twitter;

	$settings = array(
			'oauth_access_token' 		=> OAUTH_ACCESS_TOKEN,
			'oauth_access_token_secret'	=> OAUTH_ACCESS_TOKEN_SECRET,
			'consumer_key' 				=> CONSUMER_KEY,
			'consumer_secret' 			=> CONSUMER_SECRET
	);

	$twitter = new TwitterAPIExchange($settings);

	return $twitter;
}

// On every page load, check if cron job has been scheduled.
add_action( 'init', 'twitter_sync_admin_init' );
function twitter_sync_admin_init() {
	// Schedule an action if it's not already scheduled
	if ( ! wp_next_scheduled( 'sync_twitter' ) ) {
		wp_schedule_event( time(), 'twitter_sync_15_mins', 'sync_twitter' );
	}
}
